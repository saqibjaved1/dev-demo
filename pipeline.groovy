pipeline{
    agent any
    tools {
        terraform 'terraform-11'
    }
    stages{
        stage('Git checkout'){
            steps{
               git credentialsId: '1', url: 'https://bitbucket.org/saqibjaved1/dev-demo/src/master/'
            }
        }
        stage('Terraform init'){
            steps{
                sh 'terraform init'
            }
        }
        stage('Terraform Apply'){
            steps{
                sh 'terraform apply --auto-approve'
            }
        }
    }

}